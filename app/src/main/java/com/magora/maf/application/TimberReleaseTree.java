package com.magora.maf.application;

import android.util.Log;

import timber.log.Timber;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class TimberReleaseTree extends Timber.Tree {

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        switch (priority) {
            case Log.VERBOSE:
            case Log.DEBUG:
                // Ignoring
                break;
            case Log.INFO:
            case Log.WARN:
            case Log.ERROR:
            default:
                // Crashlytics.logException(t);
                break;
        }
    }
}
