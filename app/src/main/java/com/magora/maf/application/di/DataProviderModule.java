package com.magora.maf.application.di;

import com.magora.maf.model.dataprovider.ReposDataProvider;
import com.magora.maf.model.dataprovider.ReposDataProviderImpl;
import com.magora.maf.model.dataprovider.UsersDataProvider;
import com.magora.maf.model.dataprovider.UsersDataProviderImpl;
import com.magora.maf.model.datasource.repos.ReposDataSource;
import com.magora.maf.model.datasource.users.UsersDataSource;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@Module
public class DataProviderModule {

    @Singleton
    @Provides
    protected UsersDataProvider provideUsersDataProvider(
            @Named(DiConsts.KEY_CACHE) UsersDataSource usersCacheDataSource,
            @Named(DiConsts.KEY_REST) UsersDataSource usersRestDataSource) {
        return new UsersDataProviderImpl(usersCacheDataSource, usersRestDataSource);
    }

    @Singleton
    @Provides
    protected ReposDataProvider provideReposDataProvider(
            @Named(DiConsts.KEY_CACHE) ReposDataSource reposCacheDataSource,
            @Named(DiConsts.KEY_REST) ReposDataSource reposRestDataSource) {
        return new ReposDataProviderImpl(reposCacheDataSource, reposRestDataSource);
    }
}
