package com.magora.maf.application.manager;

import com.magora.maf.screen.base.mvppresenter.MvpPresenter;
import com.magora.maf.screen.base.mvprouter.MvpRouter;
import com.magora.maf.screen.base.mvpview.MvpView;

import java.util.HashMap;
import java.util.Map;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class PresenterManager {
    private static final PresenterManager ourInstance = new PresenterManager();

    private Map<Class, MvpPresenter> map = new HashMap<>();

    private PresenterManager() {
    }

    public static PresenterManager getInstance() {
        return ourInstance;
    }

    public <P extends MvpPresenter<V, R>, V extends MvpView, R extends MvpRouter> void add(Class viewClass, P presenter) {
        map.put(viewClass, presenter);
    }

    public void remove(Class viewClass) {
        map.remove(viewClass);
    }

    public <P extends MvpPresenter<V, R>, V extends MvpView, R extends MvpRouter> P get(Class viewClass) {
        //noinspection unchecked
        return (P) map.get(viewClass);
    }

    public boolean contains(Class viewClass) {
        return map.containsKey(viewClass);
    }
}
