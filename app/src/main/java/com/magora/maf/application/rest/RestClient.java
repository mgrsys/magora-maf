package com.magora.maf.application.rest;

import com.magora.maf.model.dataobject.dto.RepoDto;
import com.magora.maf.model.dataobject.dto.UserDto;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface RestClient {

    @GET("/users")
    Flowable<List<UserDto>> getUsers();

    @GET("/users/{login}")
    Flowable<UserDto> getUser(@Path("login") String login);

    @GET("/repositories")
    Flowable<List<RepoDto>> getRepos();

    @GET("/users/{login}/repos")
    Flowable<List<RepoDto>> getReposForUser(@Path("login") String login);
}
