package com.magora.maf.application.di;

import com.magora.maf.application.rest.RestClient;
import com.magora.maf.model.datasource.repos.ReposCacheDataSource;
import com.magora.maf.model.datasource.repos.ReposDataSource;
import com.magora.maf.model.datasource.repos.ReposRestDataSource;
import com.magora.maf.model.datasource.users.UsersCacheDataSource;
import com.magora.maf.model.datasource.users.UsersDataSource;
import com.magora.maf.model.datasource.users.UsersRestDataSource;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@Module
public class DataSourceModule {

    @Named(DiConsts.KEY_CACHE)
    @Singleton
    @Provides
    protected UsersDataSource provideUsersCacheDataSource() {
        return new UsersCacheDataSource();
    }

    @Named(DiConsts.KEY_REST)
    @Singleton
    @Provides
    protected UsersDataSource provideUsersRestDataSource(RestClient restClient) {
        return new UsersRestDataSource(restClient);
    }

    @Named(DiConsts.KEY_CACHE)
    @Singleton
    @Provides
    protected ReposDataSource provideReposCacheDataSource() {
        return new ReposCacheDataSource();
    }

    @Named(DiConsts.KEY_REST)
    @Singleton
    @Provides
    protected ReposDataSource provideReposRestDataSource(RestClient restClient) {
        return new ReposRestDataSource(restClient);
    }
}
