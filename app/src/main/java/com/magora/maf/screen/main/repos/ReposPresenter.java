package com.magora.maf.screen.main.repos;

import com.magora.maf.model.usecase.GetReposUseCase;
import com.magora.maf.screen.base.mvppresenter.BaseLivePresenter;
import com.magora.maf.screen.main.MainContract;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class ReposPresenter
        extends BaseLivePresenter<ReposContract.View, MainContract.Router>
        implements ReposContract.Presenter {
    private final GetReposUseCase getReposUseCase;

    public ReposPresenter(GetReposUseCase getReposUseCase) {
        this.getReposUseCase = getReposUseCase;
    }
}
