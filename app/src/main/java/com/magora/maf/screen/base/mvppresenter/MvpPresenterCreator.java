package com.magora.maf.screen.base.mvppresenter;

import com.magora.maf.screen.base.mvprouter.MvpRouter;
import com.magora.maf.screen.base.mvpview.MvpView;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface MvpPresenterCreator<P extends MvpPresenter<V, R>, V extends MvpView, R extends MvpRouter> {
    P createPresenter();
}
