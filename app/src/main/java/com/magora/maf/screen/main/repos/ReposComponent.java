package com.magora.maf.screen.main.repos;

import com.magora.maf.application.di.AppComponent;
import com.magora.maf.application.di.scope.FragmentScope;

import dagger.Component;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@FragmentScope
@Component(dependencies = AppComponent.class, modules = ReposModule.class)
public interface ReposComponent {

    void inject(ReposFragment fragment);
}
