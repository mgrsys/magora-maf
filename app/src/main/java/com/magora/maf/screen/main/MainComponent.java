package com.magora.maf.screen.main;

import com.magora.maf.application.di.AppComponent;
import com.magora.maf.application.di.scope.ActivityScope;

import dagger.Component;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@ActivityScope
@Component(dependencies = AppComponent.class, modules = MainModule.class)
public interface MainComponent {

    void inject(MainActivity activity);
}
