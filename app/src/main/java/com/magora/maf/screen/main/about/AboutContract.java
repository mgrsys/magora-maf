package com.magora.maf.screen.main.about;

import com.magora.maf.screen.base.mvppresenter.MvpPresenter;
import com.magora.maf.screen.base.mvpview.MvpView;
import com.magora.maf.screen.main.MainContract;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface AboutContract {

    interface View extends MvpView {

    }

    interface Presenter extends MvpPresenter<View, MainContract.Router> {

    }
}
