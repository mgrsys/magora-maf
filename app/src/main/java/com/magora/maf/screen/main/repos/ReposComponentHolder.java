package com.magora.maf.screen.main.repos;

import com.magora.maf.application.di.holder.BaseComponentHolder;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class ReposComponentHolder extends BaseComponentHolder<ReposComponent> {
    private static final ReposComponentHolder instance = new ReposComponentHolder();

    private ReposComponentHolder() {
    }

    public static ReposComponentHolder getInstance() {
        return instance;
    }
}
