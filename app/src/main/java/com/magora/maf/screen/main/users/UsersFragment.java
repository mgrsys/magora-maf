package com.magora.maf.screen.main.users;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.magora.maf.R;
import com.magora.maf.application.di.AppComponentHolder;
import com.magora.maf.screen.base.OnItemReselectedListener;
import com.magora.maf.screen.base.fragment.BaseLiveFragment;
import com.magora.maf.screen.base.recycler.BaseCellDelegateAdapter;
import com.magora.maf.screen.main.MainContract;

import java.util.List;

import butterknife.BindView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class UsersFragment
        extends BaseLiveFragment<UsersContract.Presenter, UsersContract.View, MainContract.Router>
        implements UsersContract.View, OnItemReselectedListener {
    @BindView(R.id.recycler_view)
    protected RecyclerView recyclerView;
    @BindView(R.id.empty_view)
    protected View emptyView;
    @BindView(R.id.progress_view)
    protected View progressView;

    private BaseCellDelegateAdapter<UserRecyclerObject> adapter;
    private UserCellDelegate userCellDelegate;
    private UserProgressCellDelegate progressCellDelegate;

    public static UsersFragment newInstance() {
        return new UsersFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        UsersComponentHolder.getInstance().bindComponent(
                DaggerUsersComponent.builder()
                        .appComponent(AppComponentHolder.getInstance().getComponent())
                        .build()
        );
        UsersComponentHolder.getInstance().getComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_users, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        initListeners();
    }

    private void initViews() {
        userCellDelegate = new UserCellDelegate();
        progressCellDelegate = new UserProgressCellDelegate();

        adapter = new BaseCellDelegateAdapter<>();
        adapter.setCellDelegates(userCellDelegate, progressCellDelegate);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }

    @SuppressWarnings("CodeBlock2Expr")
    private void initListeners() {
        userCellDelegate.setUserClickListener((itemView, position, item) -> {
            consumePresenter(p -> p.onUserDetailsClick(item));
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        UsersComponentHolder.getInstance().unbindComponent();
    }

    //region UsersContract.View
    @Override
    public void setData(List<UserRecyclerObject> recyclerObjects) {
        adapter.setItems(recyclerObjects);
    }

    @Override
    public void setProgressViewEnabled(boolean enabled) {
        progressView.setVisibility(enabled ? VISIBLE : GONE);
    }

    @Override
    public void setEmptyViewEnabled(boolean enabled) {
        emptyView.setVisibility(enabled ? VISIBLE : GONE);
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(recyclerView, message, Snackbar.LENGTH_SHORT).show();
    }
    //endregion

    //region OnItemReselectedListener
    @Override
    public void onItemReselected() {
        recyclerView.smoothScrollToPosition(0);
    }
    //endregion
}
