package com.magora.maf.screen.main.about;

import com.magora.maf.screen.base.mvppresenter.BaseLivePresenter;
import com.magora.maf.screen.main.MainContract;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class AboutPresenter
        extends BaseLivePresenter<AboutContract.View, MainContract.Router>
        implements AboutContract.Presenter {

}
