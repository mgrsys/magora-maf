package com.magora.maf.screen.main.users;

import com.magora.maf.application.di.holder.BaseComponentHolder;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class UsersComponentHolder extends BaseComponentHolder<UsersComponent> {
    private static final UsersComponentHolder instance = new UsersComponentHolder();

    private UsersComponentHolder() {
    }

    public static UsersComponentHolder getInstance() {
        return instance;
    }
}
