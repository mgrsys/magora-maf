package com.magora.maf.screen.main.users;

import com.magora.maf.model.usecase.GetUsersUseCase;
import com.magora.maf.screen.base.mvppresenter.BaseLivePresenter;
import com.magora.maf.screen.main.MainContract;
import com.magora.maf.screen.viewobject.UserVo;
import com.magora.maf.screen.viewobject.UserVoMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class UsersPresenter
        extends BaseLivePresenter<UsersContract.View, MainContract.Router>
        implements UsersContract.Presenter {
    private final GetUsersUseCase getUsersUseCase;

    public UsersPresenter(GetUsersUseCase getUsersUseCase) {
        this.getUsersUseCase = getUsersUseCase;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        requestUsers();
    }

    @SuppressWarnings("CodeBlock2Expr")
    private void requestUsers() {
        handleUseCaseLifecycle(getUsersUseCase);

        getUsersUseCase.setCallbacks(
                () -> {
                    consumeView(v -> {
                        v.setProgressViewEnabled(true);
                        v.setEmptyViewEnabled(false);
                    });
                },
                userDtos -> {
                    final List<UserVo> userVos = UserVoMapper.fromDto(userDtos);
                    consumeView(v -> {
                        v.setProgressViewEnabled(false);
                        v.setEmptyViewEnabled(false);
                        v.setData(mapUsersToRecyclerObjects(userVos));
                    });
                },
                throwable -> {
                    consumeView(v -> {
                        v.setProgressViewEnabled(false);
                        v.setEmptyViewEnabled(true);
                        v.showMessage(throwable.getLocalizedMessage());
                    });
                }
        );

        getUsersUseCase.execute();
    }

    private List<UserRecyclerObject> mapUsersToRecyclerObjects(List<UserVo> users) {
        final List<UserRecyclerObject> recyclerObjects = new ArrayList<>();
        for (UserVo user : users) {
            recyclerObjects.add(UserRecyclerObject.newInstance(user));
        }
        return recyclerObjects;
    }

    //region UsersContract.Presenter
    @Override
    public void onUserDetailsClick(UserVo user) {
        consumeRouter(r -> r.routeToUserDetails(user.getLogin()));
    }
    //endregion
}
