package com.magora.maf.screen.base.mvprouter;

import io.reactivex.functions.Consumer;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface MvpRouterConsumer<R extends MvpRouter> {
    void consumeRouter(final Consumer<R> consumer);
}
