package com.magora.maf.screen.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import com.magora.maf.R;
import com.magora.maf.application.di.AppComponentHolder;
import com.magora.maf.application.util.RoutingUtils;
import com.magora.maf.screen.base.activity.BaseActivity;

import butterknife.BindView;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class UserActivity extends BaseActivity implements UserContract.Router {
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        initDaggerComponent();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        initViews(savedInstanceState);
    }

    private void initDaggerComponent() {
        UserComponentHolder.getInstance().bindComponent(
                DaggerUserComponent.builder()
                        .appComponent(AppComponentHolder.getInstance().getComponent())
                        .build()
        );
    }

    private void initViews(@Nullable Bundle savedInstanceState) {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        RoutingUtils.showFragment(
                this,
                savedInstanceState,
                R.id.container_view_group,
                UserFragment.newInstance()
        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UserComponentHolder.getInstance().unbindComponent();
    }
}
