package com.magora.maf.screen.user;

import com.magora.maf.screen.viewobject.UserVo;
import com.magora.maf.screen.base.mvppresenter.MvpPresenter;
import com.magora.maf.screen.base.mvprouter.MvpRouter;
import com.magora.maf.screen.base.mvpview.MvpView;
import com.magora.maf.screen.base.view.EmptyView;
import com.magora.maf.screen.base.view.MessageView;
import com.magora.maf.screen.base.view.ProgressView;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface UserContract {

    interface Presenter extends MvpPresenter<View, Router> {

    }

    interface View extends MvpView, ProgressView, EmptyView, MessageView {

        void setUser(UserVo user);
    }

    interface Router extends MvpRouter {

    }
}
