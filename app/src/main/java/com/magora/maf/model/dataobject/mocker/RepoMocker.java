package com.magora.maf.model.dataobject.mocker;

import com.magora.maf.model.dataobject.dto.RepoDto;

import java.util.List;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class RepoMocker {

    public static List<RepoDto> getRepos() {
        return null;
    }
}
