package com.magora.maf.model.validator.base;

import java.util.Arrays;
import java.util.List;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class ValidatorsComposer<T> implements Validator<T> {

    private final List<Validator<T>> validators;
    private String description;

    @SafeVarargs
    public ValidatorsComposer(Validator<T>... validators) {
        this.validators = Arrays.asList(validators);
    }

    @Override
    public boolean isValid(T value) {
        for (Validator<T> validator : validators) {
            if (!validator.isValid(value)) {
                description = validator.getDescription();
                return false;
            }
        }
        return true;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
