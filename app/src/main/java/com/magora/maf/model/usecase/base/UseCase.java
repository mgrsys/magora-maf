package com.magora.maf.model.usecase.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface UseCase<In, Out> {

    void execute();

    void forceExecute();

    void abort();

    void setData(@NonNull In inputData);

    void setCallbacks(
            @Nullable Action progressCallback,
            @Nullable Consumer<Out> outputDataCallback,
            @Nullable Consumer<Throwable> errorCallback
    );
}
