package com.magora.maf.model.dataobject.dto;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
@Value
@Builder(toBuilder = true)
@EqualsAndHashCode(of = "id")
public class RepoDto {
    @SerializedName("id")
    Integer id;
    @SerializedName("name")
    String name;
    @SerializedName("full_name")
    String fullName;
    @SerializedName("owner")
    UserDto owner;
    @SerializedName("html_url")
    String htmlUrl;
    @SerializedName("description")
    String description;
}
