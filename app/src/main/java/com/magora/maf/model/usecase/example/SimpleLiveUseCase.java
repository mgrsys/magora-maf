package com.magora.maf.model.usecase.example;

import com.magora.maf.model.usecase.base.BaseLiveUseCase;

import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public class SimpleLiveUseCase extends BaseLiveUseCase<String, String> {

    @Override
    public void executeOperation() {
        final Flowable<String> f = Flowable.timer(2, TimeUnit.SECONDS)
                .map(aLong -> "Success");

        executeOperation(f);
    }
}
