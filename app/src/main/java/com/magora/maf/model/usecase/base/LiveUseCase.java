package com.magora.maf.model.usecase.base;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public interface LiveUseCase<In, Out> extends UseCase<In, Out> {

    void pushProgressEvent();

    void pushOutputDataEvent();

    void pushErrorEvent();

    void pushLastEvent();
}
