package com.magora.maf.model.dataprovider;

import com.magora.maf.model.dataobject.dto.UserDto;
import com.magora.maf.model.datasource.users.UsersDataSource;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import io.reactivex.Flowable;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class UsersDataProviderImpl implements UsersDataProvider {
    private final UsersDataSource cacheUsersDataSource;
    private final UsersDataSource restUsersDataSource;

    public UsersDataProviderImpl(UsersDataSource cacheUsersDataSource,
                                 UsersDataSource restUsersDataSource) {
        this.cacheUsersDataSource = cacheUsersDataSource;
        this.restUsersDataSource = restUsersDataSource;
    }

    @Override
    public Flowable<List<UserDto>> getUsers() {
        return Flowable.fromCallable(() -> {
            final List<UserDto> users = new ArrayList<>();
            final AtomicReference<Throwable> throwableReference = new AtomicReference<>();

            cacheUsersDataSource.getUsers().subscribe(users::addAll, throwableReference::set);

            if (users.isEmpty()) {
                throwableReference.set(null);
                restUsersDataSource.getUsers().subscribe(users::addAll, throwableReference::set);
            }

            if (throwableReference.get() != null) {
                throw new RuntimeException(throwableReference.get());
            }

            return users;
        });
    }

    @SuppressWarnings("Convert2MethodRef")
    @Override
    public Flowable<UserDto> getUser(String login) {
        return Flowable.fromCallable(() -> {
            final AtomicReference<UserDto> userReference = new AtomicReference<>();
            final AtomicReference<Throwable> throwableReference = new AtomicReference<>();

            cacheUsersDataSource.getUser(login).subscribe(userReference::set, throwableReference::set);

            if (userReference.get() == null || userReference.get().getLogin() == null) {
                throwableReference.set(null);
                restUsersDataSource.getUser(login).subscribe(userReference::set, throwableReference::set);
            }

            if (throwableReference.get() != null) {
                throw new RuntimeException(throwableReference.get());
            }

            return userReference.get();
        });
    }
}
