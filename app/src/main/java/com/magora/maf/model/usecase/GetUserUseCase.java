package com.magora.maf.model.usecase;

import com.magora.maf.model.dataobject.dto.UserDto;
import com.magora.maf.screen.viewobject.UserVo;
import com.magora.maf.model.dataprovider.UsersDataProvider;
import com.magora.maf.model.usecase.base.BaseLiveUseCase;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class GetUserUseCase extends BaseLiveUseCase<String, UserDto> {
    private final UsersDataProvider usersDataProvider;

    public GetUserUseCase(UsersDataProvider usersDataProvider) {
        this.usersDataProvider = usersDataProvider;
    }

    @Override
    protected void executeOperation() throws Exception {
        executeOperation(usersDataProvider.getUser(inputData));
    }
}
