package com.magora.maf.model.dataobject.mocker;

import com.magora.maf.model.dataobject.dto.UserDto;

import java.util.List;

/**
 * Developed by Magora Team (magora-systems.com)
 * 2017
 *
 * @author Viktor Zemtsov
 */
public final class UserMocker {

    public static UserDto getUserDto() {
        return null;
    }

    public static List<UserDto> getUserDtoList() {
        return null;
    }
}
